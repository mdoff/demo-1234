import cvsParse from 'csv-parse';
/**
 * This function will return promise that eventually return array of parsed data
 * eg. {date: "01.01.2019", datasource: "Facebook Ads", campaign: "Like Ads", clicks: 274, impressions: 1979}
 */
async function cvsDataParse(rawData) {
  return new Promise((resolve) => {
    if (!!!rawData) {
      resolve([]);
    }
    cvsParse(rawData, (error, output) => {
      // eslint-disable-next-line no-unused-vars
      const [headers, ...data] = output;
      const lowerCaseHeaders = headers.map((header) =>
        header.toLocaleLowerCase(),
      );
      const mapSingleEntityToObject = (singleEntity) =>
        lowerCaseHeaders.reduce((accumulator, element, index) => {
          const value =
            index > 2 // 3rd and 4th elements are usually a numbers
              ? Number.parseInt(singleEntity[index])
              : singleEntity[index];
          return {
            ...accumulator,
            [element]: Number.isNaN(value) ? 0 : value, // if parsed failed, use 0
          };
        }, {});

      resolve(data.map(mapSingleEntityToObject));
    });
  });
}

export default cvsDataParse;
