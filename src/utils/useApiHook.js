import { useState, useCallback } from 'react';

function useApiCall(apiCallFn) {
  const [state, setState] = useState({
    response: null,
    error: null,
    pending: false,
  });

  const callbackFn = useCallback(
    async (...args) => {
      setState({ ...state, pending: true });
      try {
        const response = await apiCallFn(...args);
        setState({ error: null, response, pending: false });
      } catch (e) {
        setState({ error: e, response: null, pending: false });
      }
    },
    [apiCallFn, state],
  );
  return [state, callbackFn];
}

export default useApiCall;
