import React from 'react';

import './Loader.css';

function Loader() {
  return (
    <div className="loader">
      <img src="/grid.svg" alt="loader" />
    </div>
  );
}

export default Loader;
