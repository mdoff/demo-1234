import React, { useMemo, useEffect } from 'react';
import PropTypes from 'prop-types';
import uniq from 'lodash/uniq';
import ReactSelect from 'react-select';

import './Filters.css';

function Filters({ data, filters, setFilters }) {
  const filtersData = useMemo(() => {
    const datasources = uniq(
      data.map((dataEntity) => dataEntity.datasource),
    ).map((singleSource) => ({ value: singleSource, label: singleSource }));

    const campaigns = uniq(data.map((dataEntity) => dataEntity.campaign)).map(
      (singleCampaign) => ({
        value: singleCampaign,
        label: singleCampaign,
      }),
    );
    return { datasources, campaigns };
  }, [data]);

  // set default filters on mount
  useEffect(() => {
    setFilters({
      campaigns: null,
      datasources: filtersData.datasources,
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="filters">
      <label htmlFor="datasources-select">Data Sources</label>
      <ReactSelect
        id="datasources-select"
        isMulti
        value={filters.datasources}
        onChange={(value) => setFilters({ ...filters, datasources: value })}
        options={filtersData.datasources}
      />

      <label htmlFor="campaigns-select">Campaigns</label>
      <ReactSelect
        id="campaigns-select"
        value={filters.campaigns}
        isMulti
        onChange={(value) => setFilters({ ...filters, campaigns: value })}
        placeholder="All Campaigns"
        options={filtersData.campaigns}
      />
    </div>
  );
}

Filters.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.shape({
      campaign: PropTypes.string,
      date: PropTypes.string,
      datasource: PropTypes.string,
      impressions: PropTypes.number,
      clicks: PropTypes.number,
    }),
  ),
  setFilters: PropTypes.func,
  filters: PropTypes.shape({
    campaigns: PropTypes.any,
    datasources: PropTypes.any,
  }),
};

export default Filters;
