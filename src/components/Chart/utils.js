export const chartOptions = {
  responsive: true,
  elements: {
    line: {
      fill: false,
    },
  },
  scales: {
    xAxes: [
      {
        display: true,
        gridLines: {
          display: false,
        },
        ticks: {
          autoSkip: true,
          maxTicksLimit: 20,
        },
      },
    ],
    yAxes: [
      {
        type: 'linear',
        display: true,
        position: 'left',
        id: 'y-axis-clicks',
        gridLines: {
          display: false,
        },
        labels: {
          show: true,
        },
      },
      {
        type: 'linear',
        display: true,
        position: 'right',
        id: 'y-axis-impressions',
        gridLines: {
          display: false,
        },
        labels: {
          show: true,
        },
      },
    ],
  },
};

export const getClicksChartData = (clicks) => ({
  label: 'Clicks',
  fill: false,
  lineTension: 0.1,
  backgroundColor: 'rgba(43,55,18,0.4)',
  borderColor: 'rgba(43,55,18,1)',
  borderCapStyle: 'butt',
  borderDash: [],
  borderDashOffset: 0.0,
  borderJoinStyle: 'miter',
  pointBorderColor: 'rgba(43,55,18,1)',
  pointBackgroundColor: '#fff',
  pointBorderWidth: 1,
  pointHoverRadius: 5,
  pointHoverBackgroundColor: 'rgba(43,55,18,1)',
  pointHoverBorderColor: 'rgba(220,220,220,1)',
  pointHoverBorderWidth: 2,
  pointRadius: 1,
  pointHitRadius: 10,
  data: clicks,
  yAxisID: 'y-axis-clicks',
});

export const getImpressionsChartData = (impressions) => ({
  label: 'Impressions',
  fill: false,
  lineTension: 0.1,
  backgroundColor: 'rgba(75,192,192,0.4)',
  borderColor: 'rgba(75,192,192,1)',
  borderCapStyle: 'butt',
  borderDash: [],
  borderDashOffset: 0.0,
  borderJoinStyle: 'miter',
  pointBorderColor: 'rgba(75,192,192,1)',
  pointBackgroundColor: '#fff',
  pointBorderWidth: 1,
  pointHoverRadius: 5,
  pointHoverBackgroundColor: 'rgba(75,192,192,1)',
  pointHoverBorderColor: 'rgba(220,220,220,1)',
  pointHoverBorderWidth: 2,
  pointRadius: 1,
  pointHitRadius: 10,
  data: impressions,
  yAxisID: 'y-axis-impressions',
});

export const filterByCampaign = (campaigns) =>
  campaigns.length === 0
    ? () => true
    : (dataEntity) => campaigns.includes(dataEntity.campaign);

export const filterByDataSources = (datasources) => (dataEntity) =>
  datasources.includes(dataEntity.datasource);

/**
 * Reduce array to object with dates as keys,
 * values of this keys are summed impressions and clicks (in this order)
 */
export const reduceToSummedObject = (
  accumulator,
  { date, clicks, impressions },
) => {
  const existingData = accumulator[date];
  if (existingData) {
    return {
      ...accumulator,
      [date]: [existingData[0] + impressions, existingData[1] + clicks],
    };
  } else {
    return {
      ...accumulator,
      [date]: [impressions, clicks],
    };
  }
};
