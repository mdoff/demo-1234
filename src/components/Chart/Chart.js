import React, { useMemo } from 'react';
import { Line } from 'react-chartjs-2';

import {
  chartOptions,
  getImpressionsChartData,
  getClicksChartData,
  filterByCampaign,
  filterByDataSources,
  reduceToSummedObject,
} from './utils';

import './Chart.css';

function Chart({ data, filters }) {
  const toDisplay = useMemo(() => {
    if (!filters.datasources) {
      // if there are no datasources selected, there will be no data
      return { labels: [], datasets: [] };
    }
    const datasources = filters.datasources.map(
      (dataSource) => dataSource.value,
    );
    const campaigns = (filters.campaigns || []).map(
      (dataSource) => dataSource.value,
    );

    const filteredData = data
      .filter(filterByCampaign(campaigns))
      .filter(filterByDataSources(datasources));

    const reducedData = filteredData.reduce(reduceToSummedObject, {});
    const labels = Object.keys(reducedData);
    const impressions = Object.values(reducedData).map(
      (dataByDate) => dataByDate[0],
    );
    const clicks = Object.values(reducedData).map(
      (dataByDate) => dataByDate[1],
    );

    return {
      labels,
      datasets: [
        getClicksChartData(clicks),
        getImpressionsChartData(impressions),
      ],
    };
  }, [filters, data]);
  return (
    <div className="chart">
      <Line data={toDisplay} options={chartOptions} />
    </div>
  );
}

export default Chart;
