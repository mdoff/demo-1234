import React, { useEffect, useState } from 'react';

import Filters from 'components/Filters/Filters';
import Chart from 'components/Chart/Chart';
import Loader from 'components/Loader/Loader';

import useApiHook from 'utils/useApiHook';
import cvsDataParse from 'utils/cvsDataParse';
import apiUrl from 'config/apiUrl';

import './App.css';

function App() {
  const [{ response, error, pending }, fetchData] = useApiHook(() =>
    fetch(apiUrl).then((response) =>
      response.text().then((rawData) => cvsDataParse(rawData)),
    ),
  );

  const [filters, setFilters] = useState({
    campaigns: null,
    datasources: null,
  });

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const isDataPresent = !!response && !pending;

  // loader
  if (!isDataPresent) {
    return <Loader />;
  }

  if (error) {
    return <div className="app error">Something went wrong</div>;
  }

  return (
    <div className="app">
      <Filters filters={filters} setFilters={setFilters} data={response} />
      <Chart filters={filters} data={response} />
    </div>
  );
}

export default App;
